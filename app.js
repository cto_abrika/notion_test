const _ = require('lodash');
const express = require('express');
const path = require('path');
const config = require('react-global-configuration');
const app = express();

if (process.env.NODE_ENV && process.env.NODE_ENV !== 'development') {
  config.set(_.merge(require('./src/config/default'), require(`./src/config/${process.env.NODE_ENV}`)));
} else {
  config.set(require('./src/config/default'));
}

app.use(express.static(path.join(__dirname, 'build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(process.env.PORT || config.get('http.port'), () => {
  console.log(
    `${new Date()} Server started on ${process.env.PORT
    || config.get('http.port')} port`,
  );
});
