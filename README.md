# notion test

### node version `^12.16.2`

## Start dev

1) Install dependencies

    `npm i`

2) Run

    `npm start`

## Start production

1) In folder `src/config` update `production.json` file

2) Install dependencies

    `npm i`

3) Make build

    `npm run build`

4) Run

    `npm run prod`

if you are using `pm2` (https://pm2.keymetrics.io/)

    `NODE_ENV=production pm2 start app.js --name test`

## Default port

By default starts on port 3000. To start on other port use

    `PORT=3001 npm start`

or 

    `PORT=3001 npm run prod`
