import React from 'react';
import { Form, Field } from 'react-final-form';
import { withTranslation } from 'react-i18next';

import PropTypes from 'prop-types';
import { Form as BForm, Button } from 'react-bootstrap';

const NoteForm = ({ onSubmit, t, data }) => (
  <Form
    onSubmit={onSubmit}
    validate={(values) => {
      const errors = {};

      if (!values.title) {
        errors.title = 'Required';
      }

      if (!values.content) {
        errors.content = 'Required';
      }
      return errors;
    }}
    initialValues={data}
    render={({ handleSubmit }) => (
      <BForm onSubmit={handleSubmit}>
        <Field
          name="title"
          render={({ input, meta }) => (
            <BForm.Group>
              <BForm.Label>{t('Title')}</BForm.Label>
              <BForm.Control type="text" {...input} />
              {meta.touched && meta.error && <span>{meta.error}</span>}
            </BForm.Group>
          )}
        />

        <Field
          name="content"
          render={({ input, meta }) => (
            <BForm.Group>
              <BForm.Label>{t('Content')}</BForm.Label>
              <BForm.Control as="textarea" rows="3" {...input} />
              {meta.touched && meta.error && <span>{meta.error}</span>}
            </BForm.Group>
          )}
        />

        <Button type="submit">{t('Save')}</Button>
      </BForm>
    )}
  />
);

NoteForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  data: PropTypes.shape({
    title: PropTypes.string,
    content: PropTypes.string,
  }),
};

NoteForm.defaultProps = {
  data: null,
};

export default withTranslation()(NoteForm);
