import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import { noteArrayType } from '../../propTypes/note';
import { pageSize } from '../../const/note';

import NoteItem from './item';

import './table.scss';

const NoteTable = ({ noteList, pageNumber, t }) => (
  <Table responsive className="note-table">
    <thead>
      <tr>
        <th width="10%"> </th>
        <th width="25%">{t('Title')}</th>
        <th width="25%">{t('Content')}</th>
        <th width="25%">{t('Last update')}</th>
        <th width="15%"> </th>
      </tr>
    </thead>
    <tbody>
      {noteList.map((note, index) => <NoteItem key={note.id} note={note} index={index + pageNumber * pageSize + 1} />)}
    </tbody>
  </Table>
);
NoteTable.propTypes = {
  noteList: noteArrayType,
  t: PropTypes.func.isRequired,
  pageNumber: PropTypes.number,
};

NoteTable.defaultProps = {
  noteList: [],
  pageNumber: 0,
};

export default withTranslation()(NoteTable);
