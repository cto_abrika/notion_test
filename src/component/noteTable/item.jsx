import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, ButtonGroup } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import { noteType } from '../../propTypes/note';
import { historyType } from '../../propTypes/router';
import { deleteNoteConfirm } from '../../store/action/note';

const NoteItem = ({
  note, index, deleteNoteConfirmCall, t, history,
}) => (
  <tr onClick={(e) => {
    e.preventDefault();
    e.stopPropagation();
    history.push(`/note-view/${note.id}`);
  }}
  >
    <td className="index">{index}</td>
    <td className="title">{note.title}</td>
    <td className="content">
      <div>{note.content}</div>
    </td>
    <td className="updated">{note.updated.format('LLL')}</td>
    <td className="actions">
      <ButtonGroup aria-label="Basic example">
        <Button
          variant="primary"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            history.push(`/note-update/${note.id}`);
          }}
        >
          {t('Update')}
        </Button>
        <Button
          variant="danger"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            deleteNoteConfirmCall(note);
          }}
        >
          {t('Delete')}
        </Button>
      </ButtonGroup>
    </td>
  </tr>
);

NoteItem.propTypes = {
  note: noteType.isRequired,
  index: PropTypes.number.isRequired,
  deleteNoteConfirmCall: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  history: historyType.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  deleteNoteConfirmCall: (data) => dispatch(deleteNoteConfirm(data)),
});

export default connect(null, mapDispatchToProps)(withTranslation()(withRouter(NoteItem)));
