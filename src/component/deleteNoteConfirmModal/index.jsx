import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Modal } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import { noteType } from '../../propTypes/note';

import { deleteNoteClear, deleteNoteReq } from '../../store/action/note';

const DeleteNoteConfirmModal = ({
  t,
  deleteNoteClearCall,
  deleteNoteReqCall,
  deleteConfirm,
  onConfirm,
}) => {
  if (!deleteConfirm) {
    return null;
  }

  return (
    <Modal show onHide={() => deleteNoteClearCall()}>
      <Modal.Header closeButton onHide={() => deleteNoteClearCall()}>
        <Modal.Title>{t('Confirm')}</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          {t('Are you sure you want to delete')}
          :
        </p>
        <p>{deleteConfirm.title}</p>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={() => deleteNoteClearCall()}>{t('Cancel')}</Button>
        <Button
          variant="primary"
          onClick={() => {
            deleteNoteReqCall(deleteConfirm.id);
            deleteNoteClearCall();
            if (onConfirm) {
              onConfirm();
            }
          }}
        >
          {t('Delete')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

DeleteNoteConfirmModal.propTypes = {
  deleteConfirm: noteType,
  t: PropTypes.func.isRequired,
  deleteNoteClearCall: PropTypes.func.isRequired,
  deleteNoteReqCall: PropTypes.func.isRequired,
  onConfirm: PropTypes.func,
};

DeleteNoteConfirmModal.defaultProps = {
  deleteConfirm: null,
  onConfirm: null,
};

const mapStateToProps = (state) => ({
  deleteConfirm: state.note.deleteConfirm,
});

const mapDispatchToProps = (dispatch) => ({
  deleteNoteClearCall: () => dispatch(deleteNoteClear()),
  deleteNoteReqCall: (id) => dispatch(deleteNoteReq(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(DeleteNoteConfirmModal));
