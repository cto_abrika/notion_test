import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import './notification.scss';
import Item from './item';

const Index = ({ notificationList }) => (
  <div className="notificationBox">
    {notificationList.slice(0, 3).map((i) => (
      <Item item={i} key={i.id} />
    ))}
  </div>
);

Index.propTypes = {
  notificationList: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string,
      type: PropTypes.string,
      callback: PropTypes.func,
    }),
  ),
};

Index.defaultProps = {
  notificationList: [],
};

const mapStateToProps = (state) => ({
  notificationList: state.notification.notificationList,
});

export default connect(mapStateToProps)(Index);
