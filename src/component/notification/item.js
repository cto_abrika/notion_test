import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Alert } from 'react-bootstrap';

import { notificationRemove } from '../../store/action/notification';
import './itemNotification.scss';

const Item = ({ item, notificationDelete, t }) => {
  if (!item) {
    return null;
  }

  const onClose = () => {
    notificationDelete(item.id);
  };

  let variant;

  switch (item.type) {
    case 'warning':
      variant = 'warning';
      break;

    case 'success':
      variant = 'success';
      break;

    default: variant = 'danger';
  }

  return (
    <Alert variant={variant} onClose={onClose} dismissible>
      {t(item.message)}
    </Alert>
  );
};

Item.propTypes = {
  item: PropTypes.shape({
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }),
  notificationDelete: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
};

Item.defaultProps = {
  item: null,
};

const mapDispatchToProps = (dispatch) => ({
  notificationDelete: (data) => dispatch(notificationRemove(data)),
});

export default connect(
  null,
  mapDispatchToProps,
)(withTranslation()(Item));
