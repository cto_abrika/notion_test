import React from 'react';
import './loader.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const CircleLoader = ({ loaderView }) => {
  if (!loaderView) {
    return null;
  }

  return (
    <div className="loader">
      <div className="pageBackground" />
      <div className="spinner-border text-success mr-2 circle " role="status" />
    </div>
  );
};

CircleLoader.propTypes = {
  loaderView: PropTypes.bool,
};

CircleLoader.defaultProps = {
  loaderView: false,
};

const mapStateToProps = (state) => ({
  loaderView: state.loader.view,
});

export default connect(mapStateToProps)(CircleLoader);
