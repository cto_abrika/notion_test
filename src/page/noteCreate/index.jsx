import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { noteType } from '../../propTypes/note';
import { historyType } from '../../propTypes/router';

import { addNoteClear, addNoteReq } from '../../store/action/note';

import NoteForm from '../../component/noteForm';

const NoteCreate = ({
  addNoteClearCall,
  addNoteReqCall,
  added,
  history,
}) => {
  useEffect(() => {
    if (added) {
      history.push('/');
    }

    addNoteClearCall();
  });

  return (
    <NoteForm onSubmit={(data) => addNoteReqCall(data)} />
  );
};

NoteCreate.propTypes = {
  added: noteType,
  addNoteClearCall: PropTypes.func.isRequired,
  addNoteReqCall: PropTypes.func.isRequired,
  history: historyType.isRequired,
};

NoteCreate.defaultProps = {
  added: null,
};

const mapStateToProps = (state) => ({
  added: state.note.added,
});

const mapDispatchToProps = (dispatch) => ({
  addNoteClearCall: () => dispatch(addNoteClear()),
  addNoteReqCall: (data) => dispatch(addNoteReq(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteCreate);
