import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Pagination, Button } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import { noteArrayType } from '../../propTypes/note';
import { marchType, historyType } from '../../propTypes/router';

import { getAllNotesReq } from '../../store/action/note';
import { pageSize } from '../../const/note';

import NoteTable from '../../component/noteTable';
import DeleteNoteConfirmModal from '../../component/deleteNoteConfirmModal';

import './main.scss';

const Main = ({
  getAllNotesReqCall,
  pageCount,
  noteList,
  match: {
    params,
  },
  history,
  t,
}) => {
  useEffect(() => {
    if (!noteList) {
      getAllNotesReqCall();
    }
  }, []);

  if (!noteList) {
    return null;
  }

  const pageNumber = (params.pageNumber ? params.pageNumber : 1) - 1;

  const pushTo = (page) => history.push(page ? `/note-list/${page + 1}` : '/');

  return (
    <>
      <Link to="/note-create">
        <Button className="create-note-button">{t('Create new note')}</Button>
      </Link>
      {noteList.length
        ? (
          <NoteTable
            noteList={noteList.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize)}
            pageNumber={pageNumber}
          />
        )
        : <h3>{t('No notes')}</h3>}
      { pageCount > 1 && (
        <div className="align-center">
          <Pagination size="sm">
            <Pagination.First disabled={pageNumber === 0} onClick={() => pushTo(0)} />
            <Pagination.Prev disabled={pageNumber === 0} onClick={() => pushTo(pageNumber - 1)} />
            <Pagination.Item active={pageNumber === 0} onClick={() => pushTo(0)}>{1}</Pagination.Item>
            { pageNumber > 3 && (<Pagination.Ellipsis disabled />)}

            { pageNumber > 2 && (<Pagination.Item onClick={() => pushTo(pageNumber - 2)}>{pageNumber - 1}</Pagination.Item>)}
            { pageNumber > 1 && (<Pagination.Item onClick={() => pushTo(pageNumber - 1)}>{pageNumber}</Pagination.Item>)}
            { pageNumber > 0 && pageNumber < (pageCount - 1) && (<Pagination.Item active>{pageNumber + 1}</Pagination.Item>)}
            { pageNumber < (pageCount - 2) && (<Pagination.Item onClick={() => pushTo(pageNumber + 1)}>{pageNumber + 2}</Pagination.Item>)}
            { pageNumber < (pageCount - 3) && (<Pagination.Item onClick={() => pushTo(pageNumber + 2)}>{pageNumber + 3}</Pagination.Item>)}

            { pageNumber < (pageCount - 4) && (<Pagination.Ellipsis disabled />)}
            <Pagination.Item active={pageNumber === (pageCount - 1)} onClick={() => pushTo(pageCount - 1)}>{pageCount}</Pagination.Item>
            <Pagination.Next disabled={pageNumber === (pageCount - 1)} onClick={() => pushTo(pageNumber + 1)} />
            <Pagination.Last disabled={pageNumber === (pageCount - 1)} onClick={() => pushTo(pageCount - 1)} />
          </Pagination>
        </div>
      )}
      <DeleteNoteConfirmModal />
    </>
  );
};

Main.propTypes = {
  getAllNotesReqCall: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  noteList: noteArrayType,
  match: marchType.isRequired,
  history: historyType.isRequired,
  pageCount: PropTypes.number.isRequired,
};

Main.defaultProps = {
  noteList: null,
};

const mapStateToProps = (state) => ({
  noteList: state.note.noteList,
  pageCount: state.note.pageCount,
});

const mapDispatchToProps = (dispatch) => ({
  getAllNotesReqCall: () => dispatch(getAllNotesReq()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Main));
