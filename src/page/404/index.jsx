import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import './error.scss';

const Error = ({ t }) => (
  <div className="center-content horizontal">
    <div className="vertical">
      <h1 className="display-1 font-weight-bold">404</h1>
      <p className="h1">
        {t('Page not found')}
      </p>
      <Link to="/" className="btn btn-primary btn-lg">
        {t('Return to website')}
      </Link>
    </div>
  </div>
);

Error.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation()(Error);
