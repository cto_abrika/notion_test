import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';

import { noteType } from '../../propTypes/note';
import { historyType, marchType } from '../../propTypes/router';

import { getNoteReq, deleteNoteConfirm, noteClear } from '../../store/action/note';
import DeleteNoteConfirmModal from '../../component/deleteNoteConfirmModal';

const reg = new RegExp('\n', 'g'); /* eslint "no-control-regex":0 */

const NoteView = ({
  getNoteReqCall,
  deleteNoteConfirmCall,
  history,
  match: {
    params,
  },
  note,
  t,
}) => {
  useEffect(() => {
    if (!note) {
      getNoteReqCall(params.id);
    }
  }, []);

  if (!note) {
    return null;
  }

  return (
    <>
      <Link to="/">
        <Button
          variant="success"
        >
          {t('Main')}
        </Button>
      </Link>
      <h2>{note.title}</h2>
      <p>{ReactHtmlParser(note.content.replace(reg, '<br>'))}</p>

      <ButtonGroup aria-label="Basic example">
        <Link to={`/note-update/${note.id}`}>
          <Button
            variant="primary"
          >
            {t('Update')}
          </Button>
        </Link>
        <Button
          variant="danger"
          onClick={() => deleteNoteConfirmCall(note)}
        >
          {t('Delete')}
        </Button>
      </ButtonGroup>

      <DeleteNoteConfirmModal onConfirm={() => history.push('/')} />
    </>
  );
};

NoteView.propTypes = {
  note: noteType,
  deleteConfirm: noteType,
  t: PropTypes.func.isRequired,
  getNoteReqCall: PropTypes.func.isRequired,
  deleteNoteConfirmCall: PropTypes.func.isRequired,
  history: historyType.isRequired,
  match: marchType.isRequired,
};

NoteView.defaultProps = {
  note: null,
  deleteConfirm: null,
};

const mapStateToProps = (state) => ({
  note: state.note.note,
  deleteConfirm: state.note.deleteConfirm,
});

const mapDispatchToProps = (dispatch) => ({
  noteClearCall: () => dispatch(noteClear()),
  getNoteReqCall: (id) => dispatch(getNoteReq(id)),
  deleteNoteConfirmCall: (data) => dispatch(deleteNoteConfirm(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(NoteView));
