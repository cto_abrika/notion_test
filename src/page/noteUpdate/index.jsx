import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { noteType } from '../../propTypes/note';
import { historyType, marchType } from '../../propTypes/router';

import { getNoteReq, updateNoteReq, noteClear } from '../../store/action/note';

import NoteForm from '../../component/noteForm';

const NoteUpdate = ({
  getNoteReqCall,
  updateNoteReqCall,
  noteClearCall,
  updated,
  history,
  match: {
    params,
  },
  note,
}) => {
  useEffect(() => {
    if (!note) {
      getNoteReqCall(params.id);
    }

    if (updated) {
      noteClearCall();
      history.push(`/note-view/${params.id}`);
    }
  });

  if (!note) {
    return null;
  }

  return (
    <NoteForm
      onSubmit={(data) => updateNoteReqCall({
        ...note,
        ...data,
      })}
      data={note}
    />
  );
};

NoteUpdate.propTypes = {
  note: noteType,
  updated: noteType,
  noteClearCall: PropTypes.func.isRequired,
  getNoteReqCall: PropTypes.func.isRequired,
  updateNoteReqCall: PropTypes.func.isRequired,
  history: historyType.isRequired,
  match: marchType.isRequired,
};

NoteUpdate.defaultProps = {
  note: null,
  updated: null,
};

const mapStateToProps = (state) => ({
  note: state.note.note,
  updated: state.note.updated,
});

const mapDispatchToProps = (dispatch) => ({
  noteClearCall: () => dispatch(noteClear()),
  getNoteReqCall: (id) => dispatch(getNoteReq(id)),
  updateNoteReqCall: (data) => dispatch(updateNoteReq(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteUpdate);
