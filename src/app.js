import React from 'react';

import Router from './router';
import Notification from './component/notification';
import CircleLoader from './component/loader';

const App = () => (
  <>
    <Router />

    <Notification />

    <CircleLoader />
  </>
);

export default App;
