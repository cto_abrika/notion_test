import _ from 'lodash';
import moment from 'moment';

import {
  GET_ALL_NOTES_REQ,
  GET_ALL_NOTES_SUCCESS,
  ADD_NOTE_REQ,
  ADD_NOTE_SUCCESS,
  ADD_NOTE_CLEAR,
  DELETE_NOTE_CLEAR,
  DELETE_NOTE_CONFIRM,
  DELETE_NOTE_REQ,
  DELETE_NOTE_SUCCESS,
  GET_NOTE_REQ,
  GET_NOTE_SUCCESS,
  UPDATE_NOTE_REQ,
  UPDATE_NOTE_SUCCESS,
  NOTE_CLEAR,
} from '../constant/note';

export const updateNoteReq = (data) => ({
  type: UPDATE_NOTE_REQ,
  data: _.omit(data, ['created', 'updated']),
});

export const updateNoteSuccess = (data) => ({
  type: UPDATE_NOTE_SUCCESS,
  data: {
    ...data,
    created: moment(data.created),
    updated: moment(data.updated),
  },
});

export const noteClear = () => ({
  type: NOTE_CLEAR,
});

export const getNoteReq = (id) => ({
  type: GET_NOTE_REQ,
  id,
});

export const getNoteSuccess = (data) => ({
  type: GET_NOTE_SUCCESS,
  data: {
    ...data,
    created: moment(data.created),
    updated: moment(data.updated),
  },
});

export const deleteNoteReq = (id) => ({
  type: DELETE_NOTE_REQ,
  id,
});

export const deleteNoteSuccess = (id) => ({
  type: DELETE_NOTE_SUCCESS,
  id,
});

export const deleteNoteConfirm = (data) => ({
  type: DELETE_NOTE_CONFIRM,
  data,
});

export const deleteNoteClear = () => ({
  type: DELETE_NOTE_CLEAR,
});

export const addNoteReq = (data) => ({
  type: ADD_NOTE_REQ,
  data,
});

export const addNoteSuccess = (data) => ({
  type: ADD_NOTE_SUCCESS,
  data: {
    ...data,
    created: moment(data.created),
    updated: moment(data.updated),
  },
});

export const addNoteClear = () => ({
  type: ADD_NOTE_CLEAR,
});

export const getAllNotesReq = () => ({
  type: GET_ALL_NOTES_REQ,
});

export const getAllNotesSuccess = (list) => ({
  type: GET_ALL_NOTES_SUCCESS,
  list: _.map(list, (note) => ({
    ...note,
    created: moment(note.created),
    updated: moment(note.updated),
  })),
});
