import {
  NOTIFICATION_WARNING,
  NOTIFICATION_SUCCESS,
  NOTIFICATION_INFO,
  NOTIFICATION_ERROR,
  NOTIFICATION_REMOVE,
  NOTIFICATION_ADD_LIST,
  NOTIFICATION_CLEAR_LIST,
} from '../constant/notification';

export const notificationError = (data) => ({
  type: NOTIFICATION_ERROR,
  data: {
    ...data,
    type: 'error',
  },
});

export const notificationSuccess = (data) => ({
  type: NOTIFICATION_SUCCESS,
  data: {
    ...data,
    type: 'success',
  },
});

export const notificationWarning = (data) => ({
  type: NOTIFICATION_WARNING,
  data: {
    ...data,
    type: 'warning',
  },
});

export const notificationInfo = (data) => ({
  type: NOTIFICATION_INFO,
  data: {
    ...data,
    type: 'info',
  },
});

export const notificationRemove = (id) => ({
  type: NOTIFICATION_REMOVE,
  id,
});

export const addNotificationList = (data = []) => ({
  type: NOTIFICATION_ADD_LIST,
  data,
});

export const clearNotificationList = () => ({
  type: NOTIFICATION_CLEAR_LIST,
});
