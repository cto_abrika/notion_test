import { SHOW_LOADER, HIDE_LOADER } from '../constant/loader';

export const showLoader = (operation) => ({
  type: SHOW_LOADER,
  operation,
});

export const hideLoader = (operation) => ({
  type: HIDE_LOADER,
  operation,
});
