import _ from 'lodash';
import moment from 'moment';

import {
  DB_CONNECTION,
} from '../constant/system';

export const setDBConnectionStatus = (status) => ({
  type: DB_CONNECTION,
  status,
});
