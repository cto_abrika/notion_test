import { takeLeading, call, put } from 'redux-saga/effects';
import uuid from 'uuid/v4';
import i18n from 'i18next';

import {
  GET_ALL_NOTES_REQ,
  ADD_NOTE_REQ,
  DELETE_NOTE_REQ,
  GET_NOTE_REQ,
  UPDATE_NOTE_REQ,
} from '../constant/note';

import { hideLoader, showLoader } from '../action/loader';
import {
  getAllNotesSuccess,
  addNoteSuccess,
  deleteNoteSuccess,
  getNoteSuccess,
  updateNoteSuccess,
} from '../action/note';
import { notificationError } from '../action/notification';

import indexdDB from '../../util/indexdDB';

function* getAllNotes() {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    const res = yield call(indexdDB.getAll);

    if (res) {
      yield put(getAllNotesSuccess(res));
    } else {
      yield put(
        notificationError({
          message: i18n.t('Failed to get data'),
        }),
      );
    }
  } catch (e) {
    console.error(e);
    yield put(
      notificationError({
        message: i18n.t('Failed to get data'),
      }),
    );
  }

  yield put(hideLoader(operationId));
}

function* getNote({ id }) {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    const res = yield call(indexdDB.get, +id);

    if (res) {
      yield put(getNoteSuccess(res));
    } else {
      yield put(
        notificationError({
          message: i18n.t('Failed to get data'),
        }),
      );
    }
  } catch (e) {
    console.error(e);
    yield put(
      notificationError({
        message: i18n.t('Failed to get data'),
      }),
    );
  }

  yield put(hideLoader(operationId));
}

function* deleteNote({ id }) {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    yield call(indexdDB.delete, id);

    yield put(deleteNoteSuccess(id));
  } catch (e) {
    console.error(e);
    yield put(
      notificationError({
        message: i18n.t('Failed to delete data'),
      }),
    );
  }

  yield put(hideLoader(operationId));
}

function* addNote({ data }) {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    const res = yield call(indexdDB.add, data);

    if (res) {
      const note = yield call(indexdDB.get, res);
      yield put(addNoteSuccess(note));
    } else {
      yield put(
        notificationError({
          message: i18n.t('Failed to add note'),
        }),
      );
    }
  } catch (e) {
    console.error(e);
    yield put(
      notificationError({
        message: i18n.t('Failed to add note'),
      }),
    );
  }

  yield put(hideLoader(operationId));
}

function* updateNote({ data }) {
  const operationId = uuid();
  yield put(showLoader(operationId));

  try {
    const res = yield call(indexdDB.put, data);

    if (res) {
      const note = yield call(indexdDB.get, res);
      yield put(updateNoteSuccess(note));
    } else {
      yield put(
        notificationError({
          message: i18n.t('Failed to update note'),
        }),
      );
    }
  } catch (e) {
    console.error(e);
    yield put(
      notificationError({
        message: i18n.t('Failed to update note'),
      }),
    );
  }

  yield put(hideLoader(operationId));
}

export default [
  takeLeading(GET_ALL_NOTES_REQ, getAllNotes),
  takeLeading(ADD_NOTE_REQ, addNote),
  takeLeading(DELETE_NOTE_REQ, deleteNote),
  takeLeading(GET_NOTE_REQ, getNote),
  takeLeading(UPDATE_NOTE_REQ, updateNote),
];
