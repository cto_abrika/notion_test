import { all } from 'redux-saga/effects';

import note from './note';

export default function* rootSaga() {
  yield all([
    ...note,
  ]);
}
