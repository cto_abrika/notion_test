import _ from 'lodash';

import { DB_CONNECTION } from '../constant/system';

const initial = {
  dbConnected: false,
};

const systemReducer = (state = initial, action) => {
  switch (action.type) {
    case DB_CONNECTION:
      return {
        ...state,
        dbConnected: action.status,
      };

    default:
      return state;
  }
};

export default systemReducer;
