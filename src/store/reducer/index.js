import { combineReducers } from 'redux';
import loader from './loader';
import note from './note';
import notification from './notification';
import system from './system';

export default combineReducers({
  loader,
  notification,
  note,
  system,
});
