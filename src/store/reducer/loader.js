import { SHOW_LOADER, HIDE_LOADER } from '../constant/loader';

const initial = {
  operationList: [],
  view: false,
};

const loaderReducer = (state = initial, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return {
        ...state,
        operationList: [...state.operationList, action.operation],
        view: true,
      };

    case HIDE_LOADER: {
      const operationList = state.operationList.filter(
        (operation) => operation !== action.operation,
      );

      return {
        ...state,
        operationList: [...operationList],
        view: !!operationList.length,
      };
    }

    default:
      return state;
  }
};

export default loaderReducer;
