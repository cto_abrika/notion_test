import uuid from 'uuid/v4';
import _ from 'lodash';

import {
  NOTIFICATION_ERROR,
  NOTIFICATION_INFO,
  NOTIFICATION_SUCCESS,
  NOTIFICATION_WARNING,
  NOTIFICATION_REMOVE,
  NOTIFICATION_ADD_LIST,
  NOTIFICATION_CLEAR_LIST,
} from '../constant/notification';

const initial = {
  notificationList: [],
};

const notificationReducer = (state = initial, action) => {
  switch (action.type) {
    case NOTIFICATION_INFO:
    case NOTIFICATION_SUCCESS:
    case NOTIFICATION_WARNING:
    case NOTIFICATION_ERROR: {
      const data = {
        ...action.data,
        id: uuid(),
        createdAt: Date.now(),
      };
      return {
        ...state,
        notificationList: [data, ...state.notificationList],
      };
    }

    case NOTIFICATION_REMOVE: {
      const list = state.notificationList;

      _.remove(list, { id: action.id });

      return {
        ...state,
        notificationList: [...list],
      };
    }

    case NOTIFICATION_ADD_LIST: {
      const notificationList = [
        ...state.notificationList,
        action.data.map((item) => ({
          ...item,
          id: uuid(),
          createdAt: Date.now(),
          type: item.type ? item.type : 'error',
        })),
      ];

      return {
        ...state,
        notificationList,
      };
    }

    case NOTIFICATION_CLEAR_LIST: {
      return {
        ...state,
        notificationList: [],
      };
    }

    default:
      return state;
  }
};

export default notificationReducer;
