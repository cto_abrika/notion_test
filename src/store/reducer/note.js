import _ from 'lodash';

import { pageSize } from '../../const/note';
import {
  GET_ALL_NOTES_SUCCESS,
  ADD_NOTE_CLEAR,
  ADD_NOTE_SUCCESS,
  DELETE_NOTE_CONFIRM,
  DELETE_NOTE_CLEAR,
  DELETE_NOTE_SUCCESS,
  GET_NOTE_SUCCESS,
  UPDATE_NOTE_SUCCESS,
  NOTE_CLEAR,
} from '../constant/note';

const initial = {
  noteList: null,
  pageCount: 0,
  added: null,
  updated: null,
  deleteConfirm: null,
  note: null,
};

const sort = (list) => _.sortBy(list, (note) => (-note.updated.unix()));
const getPageCount = (list) => {
  let count = Math.trunc(list.length / pageSize);

  if (list.length % pageSize) {
    count += 1;
  }

  return count;
};

const noteReducer = (state = initial, action) => {
  switch (action.type) {
    case NOTE_CLEAR:
      return {
        ...state,
        updated: null,
        note: null,
      };

    case UPDATE_NOTE_SUCCESS: {
      if (!state.noteList) {
        return {
          ...state,
          updated: action.data,
        };
      }

      const list = state.noteList.filter((note) => note.id !== action.data.id);
      list.push(action.data);

      return {
        ...state,
        updated: action.data,
        noteList: sort(list),
      };
    }

    case GET_NOTE_SUCCESS:

      return {
        ...state,
        note: action.data,
      };

    case DELETE_NOTE_SUCCESS: {
      if (!state.noteList) {
        return state;
      }

      const list = state.noteList.filter((note) => note.id !== action.id);

      return {
        ...state,
        noteList: sort(list),
        pageCount: getPageCount(list),
      };
    }

    case DELETE_NOTE_CLEAR:
      return {
        ...state,
        deleteConfirm: null,
      };

    case DELETE_NOTE_CONFIRM:
      return {
        ...state,
        deleteConfirm: action.data,
      };

    case ADD_NOTE_SUCCESS: {
      if (!state.noteList) {
        return {
          ...state,
          added: action.data,
        };
      }

      const list = [...state.noteList, action.data];

      return {
        ...state,
        added: action.data,
        noteList: sort(list),
        pageCount: getPageCount(list),
      };
    }

    case ADD_NOTE_CLEAR:
      return {
        ...state,
        added: null,
      };

    case GET_ALL_NOTES_SUCCESS:
      return {
        ...state,
        noteList: sort(action.list),
        pageCount: getPageCount(action.list),
      };

    default:
      return state;
  }
};

export default noteReducer;
