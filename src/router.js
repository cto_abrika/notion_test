import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Error from './page/404/index';

import Main from './page/main';
import NoteCreate from './page/noteCreate';
import NoteUpdate from './page/noteUpdate';
import NoteView from './page/noteView';

const Router = ({ dbConnected }) => {
  if (!dbConnected) {
    return null;
  }

  return (
    <Switch>
      <Route
        component={Main}
        path="/"
        exact
      />
      <Route
        component={Main}
        path="/note-list/:pageNumber"
        exact
      />
      <Route
        component={NoteCreate}
        path="/note-create"
        exact
      />
      <Route
        component={NoteUpdate}
        path="/note-update/:id"
        exact
      />
      <Route
        component={NoteView}
        path="/note-view/:id"
        exact
      />
      <Route component={Error} />
    </Switch>
  );
};

Router.propTypes = {
  dbConnected: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  dbConnected: state.system.dbConnected,
});

export default connect(mapStateToProps)(Router);
