import PropTypes from 'prop-types';

export const noteType = PropTypes.shape({
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  updated: PropTypes.object.isRequired,
  created: PropTypes.object.isRequired,
});

export const noteArrayType = PropTypes.arrayOf(noteType);
