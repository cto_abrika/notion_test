import PropTypes from 'prop-types';

export const marchType = PropTypes.shape({
  path: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  isExact: PropTypes.bool.isRequired,
  params: PropTypes.object.isRequired,
});

export const historyType = PropTypes.shape({
  push: PropTypes.func.isRequired,
});
