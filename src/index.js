import React from 'react';
import _ from 'lodash';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import config from 'react-global-configuration';

import 'bootstrap/dist/css/bootstrap.min.css';

import './index.scss';

import * as serviceWorker from './serviceWorker';

import './i18n';

if (process.env.NODE_ENV && process.env.NODE_ENV !== 'development') {
  config.set(
    _.merge(
      require('./config/default'),
      require(`./config/${process.env.NODE_ENV}`),
    ),
  );
} else {
  config.set(require('./config/default'));
}

const store = require('./store').default;
const App = require('./app').default;

require('./util/indexdDB').default.init();

require('moment/min/locales.min');

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);

serviceWorker.register();
