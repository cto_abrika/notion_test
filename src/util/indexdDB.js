import uuid from 'uuid/v4';
import i18n from 'i18next';

import store from '../store';
import dbConst from '../const/db';
import { hideLoader, showLoader } from '../store/action/loader';
import { notificationError } from '../store/action/notification';
import { setDBConnectionStatus } from '../store/action/system';

let indexedDB;

let dbConnection;

/* There are many components for working with indexdDB, I just wanted to show that I can work at this level */

class inexedDB {
  init() {
    const operationId = uuid();
    store.dispatch(showLoader(operationId));

    try {
      indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

      if (!indexedDB) {
        store.dispatch(
          notificationError({
            message: i18n.t('Failed to access data'),
          }),
        );
        return;
      }

      const request = indexedDB.open(dbConst.dbName, 1);
      request.onupgradeneeded = () => {
        const db = request.result;

        if (!db.objectStoreNames.contains(dbConst.note)) {
          db.createObjectStore(dbConst.note, {
            keyPath: 'id',
            autoIncrement: true,
          });
        }
      };

      request.onerror = () => {
        store.dispatch(
          notificationError({
            message: request.error,
          }),
        );
      };

      request.onsuccess = () => {
        dbConnection = request.result;

        store.dispatch(hideLoader(operationId));
        store.dispatch(setDBConnectionStatus(true));
      };
    } catch (e) {
      store.dispatch(
        notificationError({
          message: i18n.t('Failed to access data'),
        }),
      );
    }
  }

  add(data, node = dbConst.note) {
    return (new Promise((res, rej) => {
      const dataObject = {
        ...data,
        created: new Date(),
        updated: new Date(),
      };

      const request = dbConnection
        .transaction(node, 'readwrite')
        .objectStore(node)
        .add(dataObject);

      request.onsuccess = () => {
        res(request.result);
      };

      request.onerror = () => {
        rej(request.error);
      };
    }));
  }

  put(data, node = dbConst.note) {
    return (new Promise((res, rej) => {
      const dataObject = {
        ...data,
        updated: new Date(),
      };

      const request = dbConnection
        .transaction(node, 'readwrite')
        .objectStore(node)
        .put(dataObject);

      request.onsuccess = () => {
        res(request.result);
      };

      request.onerror = () => {
        rej(request.error);
      };
    }));
  }

  delete(key, node = dbConst.note) {
    return (new Promise((res, rej) => {
      const request = dbConnection
        .transaction(node, 'readwrite')
        .objectStore(node)
        .delete(key);

      request.onsuccess = () => {
        res(request.result);
      };

      request.onerror = () => {
        rej(request.error);
      };
    }));
  }

  getAll(node = dbConst.note) {
    return (new Promise((res, rej) => {
      const request = dbConnection
        .transaction(node, 'readwrite')
        .objectStore(node)
        .getAll();

      request.onsuccess = () => {
        res(request.result);
      };

      request.onerror = () => {
        rej(request.error);
      };
    }));
  }

  get(key, node = dbConst.note) {
    return (new Promise((res, rej) => {
      const request = dbConnection
        .transaction(node, 'readwrite')
        .objectStore(node)
        .get(key);

      request.onsuccess = () => {
        res(request.result);
      };

      request.onerror = () => {
        rej(request.error);
      };
    }));
  }
}

export default new inexedDB();
